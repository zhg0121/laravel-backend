<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BaseController as BaseController;
use Dotenv\Exception\ValidationException;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;


class MemberController extends BaseController
{
    public function index()
    {
        $member = [
            'mail' => Auth::user()->email,
            'password' => Auth::user()->password,
        ];

        return $member;
    }

    public function store(Request $request)
    {

        $request->validate([
            'email' => ['required', 'string', 'email', 'unique:members'],
            'password' => ['required', 'string', 'min:6', 'max:12'],
            'name' => ['required', 'string'],
        ]);



        $hashPassword = Crypt::encryptString($request['password'] . $request['name']);
        // echo $hashPassword."\n";
        $apiToken = Str::random(10);
        $create = Member::create([
            'email' => $request['email'],
            'password' => $hashPassword,
            'name' => $request['name'],
            'api_token' => $apiToken
        ]);

        if ($create)
            // return "Register as a normal user. Your api token is $apiToken";
            return BaseController::sendResponse('Register Success!');
    }
}
