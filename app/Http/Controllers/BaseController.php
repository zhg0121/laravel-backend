<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller
{
    //

    public function sendResponse($result, $code=200)
    {
        $response = [
            'success' => 'true',
            'message' => $result,
        ];

        return response()->json($response, $code);
    }

    public function sendError($error, $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];
        
        return response()->json($response, $code);
    }
}
