<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Support\Facades\Crypt;


class LoginController extends BaseController
{
    //
    public function login(Request $request)
    {

        $request->validate([
            'email' => ['required'],
            'password' => ['required'],
        ]);

        // $loginStatus = session()->get('loginStatus');
        // if ($loginStatus == 1) {
        //     return BaseController::sendError('You had login');
        // }

        $member = Member::where('email', $request['email'])->first();

        // echo $member;

        if (!$member) {
            return BaseController::sendError("You had not registered yet.");
            // return response()->json([
            //     'status' => 'error',
            //     'message' => "You had not registered yet."
            // ]);
        } else {
            // 密碼解密
            $checkPwd = Crypt::decryptString($member['password']);

            // 密碼比對
            if ($checkPwd == $request['password'] . $member['name']) {
                session()->put('loginStatus' . $member['id'], 1);

                // echo session()->all() . "\n";

                return BaseController::sendResponse('Login Success');

                // return response()->json([
                //     'status' => 'Success',
                //     'message' => "Login Success."
                // ]);
            } else {
                return BaseController::sendError('You entered a wrong password.');
                // return response()->json([
                //     'status' => 'error',
                //     'message' => "You entered a wrong password."
                // ]);
            }
        }
    }

    public function logout(Request $request)
    {
        $member = Member::where('email', $request['email'])->first();
        // $loginStatus = session()->get('loginStatus');
        // if ($loginStatus == 0) {
        //     return BaseController::sendError('You had not login');
        // }

        session()->put('loginStatus', 0);
        return BaseController::sendResponse('You had logout');
    }

    public function testSession(Request $request)
    {
        return $request;
    }
}
